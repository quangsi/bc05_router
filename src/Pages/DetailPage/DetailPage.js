import axios from "axios";
import React, { Component } from "react";
import Header from "../../Components/Header/Header";
import { createConfigHeader } from "../../utils/utils";
import { Progress } from "antd";
export default class DetailPage extends Component {
  state = {
    detail: {},
  };
  componentDidMount() {
    console.log(this.props);
    let { id } = this.props.match.params;

    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: createConfigHeader(),
    })
      .then((res) => {
        this.setState({ detail: res.data.content });
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    let { hinhAnh, danhGia } = this.state.detail;
    return (
      <div>
        <img
          className="w-50 "
          style={{ height: "50vh", objectFit: "cover" }}
          src={hinhAnh}
        />
        <Progress
          type="circle"
          percent={danhGia * 10}
          format={(number) => {
            return `${number / 10} điểm`;
          }}
        />
      </div>
    );
  }
}
