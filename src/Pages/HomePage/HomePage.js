import React, { Component } from "react";
import axios from "axios";
import { createConfigHeader, TOKEN_CYBERSOFT } from "../../utils/utils";
import MovieItem from "./MovieItem";
export default class HomePage extends Component {
  state = {
    movieArr: [],
  };
  componentDidMount() {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03",
      method: "GET",
      headers: createConfigHeader(),
    })
      .then((res) => {
        console.log(res);
        this.setState({ movieArr: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  renderMovieList() {
    return this.state.movieArr.map((item, index) => {
      return (
        <div className="col-2" key={index}>
          <MovieItem movie={item} />
        </div>
      );
    });
  }
  render() {
    return (
      <div className="container">
        <div0 className="row">{this.renderMovieList()}</div0>
      </div>
    );
  }
}
// npm i react-router-dom@5.3.4

// renderMovieList()
// props : movie
/**
 * {
    "maPhim": 10351,
    "tenPhim": "Avatar: The Way Of Water",
    "biDanh": "avatar-the-way-of-water",
    "trailer": "https://www.youtube.com/embed/oeRG9A6bDdY",
    "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/avatar-the-way-of-water_gp03.jpg",
    "moTa": "Câu chuyện của “Avatar: Dòng Chảy Của Nước” lấy bối cảnh 10 năm sau những sự kiện xảy ra ở phần đầu tiên. Phim kể câu chuyện về gia đình mới của Jake Sully (Sam Worthington thủ vai) cùng những rắc rối theo sau và bi kịch họ phải chịu đựng khi phe loài người xâm lược hành tinh Pandora.",
    "maNhom": "GP03",
    "ngayKhoiChieu": "2022-10-09T00:00:00",
    "danhGia": 10,
    "hot": true,
    "dangChieu": true,
    "sapChieu": false
}
 */
