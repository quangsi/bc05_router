import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class MovieItem extends Component {
  render() {
    let { hinhAnh, tenPhim, moTa, maPhim } = this.props.movie;
    return (
      <div className="card text-left h-100 text-center">
        <img
          style={{ height: "350px", objectFit: "cover" }}
          className="card-img-top"
          src={hinhAnh}
          alt
        />
        <div className="card-body">
          <h4 className="card-title">{tenPhim}</h4>
          <p className="card-text">
            {moTa.length < 60 ? moTa : moTa.slice(0, 60) + "..."}
          </p>
        </div>
        <NavLink to={`/detail/${maPhim}`}>
          <button className="btn  btn-danger">Xem chi tiết</button>
        </NavLink>
      </div>
    );
  }
}
