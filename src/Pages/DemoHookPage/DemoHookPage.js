import React, { useEffect, useState } from "react";
import Banner from "./Banner";

export default function DemoHookPage() {
  let [like, setLike] = useState(1);
  let [share, setShare] = useState(1);
  //
  useEffect(() => {
    // didMount, didUpdate, willUnmount
    console.log("did mount");
  }, [like, share]);

  let handlePlusLike = () => {
    setLike(like + 1);
  };
  let handlePlusShare = () => {
    setShare(share + 1);
  };
  console.log("render");
  return (
    <div>
      <h2>DemoHookPage</h2>
      <span className="display-4">Like:{like}</span>
      <button onClick={handlePlusLike} className="btn btn-success">
        Plus like
      </button>
      <br />
      <span className="display-4">Share:{share}</span>
      <button onClick={handlePlusShare} className="btn btn-secondary">
        Plus share
      </button>
      {like < 10 && (
        <Banner
          title="Hello 
      CyberSoft"
          handleClick={handlePlusLike}
        />
      )}
    </div>
  );
}
// rfc /hook

// useMemo
// useCallback
