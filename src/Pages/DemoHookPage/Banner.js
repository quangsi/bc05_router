import React, { memo, useEffect } from "react";

function Banner({ title, handleClick }) {
  useEffect(() => {
    return () => {
      console.log("Will unmount");
    };
    // willUnmount
  }, []);
  console.log("Banner render");
  return (
    <div className="p-5 bg-dark text-white">
      Banner
      <h2>{title}</h2>
      <button onClick={handleClick} className="btn btn-light">
        Plus like
      </button>
    </div>
  );
}

export default memo(Banner);
