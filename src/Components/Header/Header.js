import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Header extends Component {
  render() {
    return (
      <div className=" p-5 text-left">
        <button className="btn btn-warning mx-5">
          <NavLink
            activeStyle={{
              color: "black",
              fontSize: 30,
            }}
            exact
            to={"/"}
          >
            Home
          </NavLink>
        </button>
        <button className="btn btn-warning">
          <NavLink
            activeStyle={{
              color: "black",
              fontSize: 30,
            }}
            to={"/hook"}
          >
            Hook
          </NavLink>
        </button>
      </div>
    );
  }
}
