import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import "antd/dist/reset.css";
import DemoHookPage from "./Pages/DemoHookPage/DemoHookPage";
import Header from "./Components/Header/Header";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/detail/:id" component={DetailPage} />

          <Route
            path="/hook"
            render={() => {
              return <DemoHookPage />;
            }}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
